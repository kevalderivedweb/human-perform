package com.example.humanperform.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.humanperform.R;

public class AdapterCreateGroup extends RecyclerView.Adapter<AdapterCreateGroup.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;

    public AdapterCreateGroup(Context mContext, OnItemClickListener listener) {
        this.listener = listener;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_create_group, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });


        /*if (position % 2 == 0) {

            if (holder.itemView.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
                ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) holder.itemView.getLayoutParams();
                p.setMargins(0, 15, 15, 15);
                holder.itemView.requestLayout();
            }
        } else {
            if (holder.itemView.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
                ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) holder.itemView.getLayoutParams();
                p.setMargins(15, 15, 0, 15);
                holder.itemView.requestLayout();
            }
        }
*/

    }

    @Override
    public int getItemCount() {
        return 8;
    }

    public class Viewholder extends RecyclerView.ViewHolder {


        public Viewholder(@NonNull View itemView) {
            super(itemView);


        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }
}