package com.example.humanperform.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.humanperform.R;

public class AdapterBookPeoplesList extends RecyclerView.Adapter<AdapterBookPeoplesList.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    int selectedPosition=-1;


    public AdapterBookPeoplesList(Context mContext, OnItemClickListener listener) {
        this.listener = listener;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_book_peoples_list, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });


        if (selectedPosition == position) {
            holder.layoutCust.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.color_round_pink_10dp));
        }
        else {
            holder.layoutCust.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.color_round_gray_10dp));
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition=position;
                notifyDataSetChanged();

            }
        });


    }

    @Override
    public int getItemCount() {
        return 8;
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        LinearLayout layoutCust;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            layoutCust = itemView.findViewById(R.id.layoutCust);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }
}