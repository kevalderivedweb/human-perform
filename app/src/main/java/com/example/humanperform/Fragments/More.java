package com.example.humanperform.Fragments;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.example.humanperform.Activity.ChangePassowrdActivity;
import com.example.humanperform.Activity.LoginOptionActivity;
import com.example.humanperform.Activity.PrivacyPolicyActivity;
import com.example.humanperform.Activity.Terms_condition_Activity;
import com.example.humanperform.R;

public class More extends Fragment {



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.more, container, false);

        view.findViewById(R.id.edit_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openEditDialog();
            }
        });

        view.findViewById(R.id.chnage_password).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ChangePassowrdActivity.class));
            }
        });

        view.findViewById(R.id.privacy_policy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), PrivacyPolicyActivity.class));
            }
        });

        view.findViewById(R.id.terms).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), Terms_condition_Activity.class));
            }
        });

        view.findViewById(R.id.logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), LoginOptionActivity.class));
                getActivity().finish();

            }
        });

        return view;
    }

    private void openEditDialog() {
        Dialog dialogForCity;
        dialogForCity = new Dialog(getContext());
        dialogForCity.setContentView(R.layout.pf_edit_profile_dg);
        dialogForCity.setCancelable(true);
        dialogForCity.setCanceledOnTouchOutside(true);
        dialogForCity.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialogForCity.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);


        dialogForCity.findViewById(R.id.dialogClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialogForCity.dismiss();

            }
        });


        dialogForCity.show();
    }


}