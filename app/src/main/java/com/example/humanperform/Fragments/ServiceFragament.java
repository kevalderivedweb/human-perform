package com.example.humanperform.Fragments;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.humanperform.Activity.ServiceInner;
import com.example.humanperform.Adapter.AdapterService;
import com.example.humanperform.R;

public class ServiceFragament extends Fragment {

    private RecyclerView recService;
    private AdapterService adapterService;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.service_fragament, container, false);


        recService = view.findViewById(R.id.recService);
        recService.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterService = new AdapterService(getContext(), new AdapterService.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {
                startActivity(new Intent(getContext(), ServiceInner.class));
            }
        });
        recService.setAdapter(adapterService);



        return view;
    }


}