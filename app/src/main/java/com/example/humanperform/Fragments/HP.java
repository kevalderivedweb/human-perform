package com.example.humanperform.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.humanperform.Activity.Filter_HP;
import com.example.humanperform.R;

import org.w3c.dom.Text;

public class HP extends Fragment {


    private TextView mMorning,mAfternoon;
    private LinearLayout mFilter;
    private LinearLayout hp;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.hp, container, false);

        mMorning = view.findViewById(R.id.morning);
        mAfternoon = view.findViewById(R.id.afternoon);
        mFilter = view.findViewById(R.id.filter);
        hp = view.findViewById(R.id.hp);

        mMorning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMorning.setBackground(getActivity().getResources().getDrawable(R.drawable.bottamline_fill));
                mAfternoon.setBackground(getActivity().getResources().getDrawable(R.drawable.bottamline_unfill));
                mMorning.setPadding(5,5,5,5);
                mAfternoon.setPadding(5,5,5,5);
            }
        });
        mAfternoon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAfternoon.setBackground(getActivity().getResources().getDrawable(R.drawable.bottamline_fill));
                mMorning.setBackground(getActivity().getResources().getDrawable(R.drawable.bottamline_unfill));
                mMorning.setPadding(5,5,5,5);
                mAfternoon.setPadding(5,5,5,5);


            }
        });


        mFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Filter_HP.class);
                startActivity(intent);
            }
        });





        return view;
    }


}