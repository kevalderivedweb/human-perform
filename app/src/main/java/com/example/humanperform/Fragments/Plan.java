package com.example.humanperform.Fragments;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CalendarView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.humanperform.Activity.NewBooking1;
import com.example.humanperform.Adapter.AdapterPeoplesName;
import com.example.humanperform.R;

import java.util.Calendar;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarView;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;

public class Plan extends Fragment {


    private TextView month;

    private RecyclerView recPeoplesName;
    private AdapterPeoplesName adapterPeoplesName;

    private LinearLayout weeklyBook, yearlyBook;
    private TextView weeklyText, yearlyText;
    private HorizontalCalendarView calendarView;
    private CalendarView calendarView_year;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.plan, container, false);

        month = view.findViewById(R.id.monthYear);
        recPeoplesName = view.findViewById(R.id.recPeoplesName);
        calendarView = view.findViewById(R.id.calendarView);
        calendarView_year = view.findViewById(R.id.calendarView_year);


        weeklyBook = view.findViewById(R.id.weeklyBook);
        yearlyBook = view.findViewById(R.id.yearlyBook);
        weeklyText = view.findViewById(R.id.weeklyText);
        yearlyText = view.findViewById(R.id.yearlyText);

        calendarView.setVisibility(View.VISIBLE);
        calendarView_year.setVisibility(View.GONE);


        weeklyBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                calendarView.setVisibility(View.VISIBLE);
                calendarView_year.setVisibility(View.GONE);
                weeklyBook.setBackground(getResources().getDrawable(R.drawable.corner_round_5));
                yearlyBook.setBackground(getResources().getDrawable(R.drawable.corner_round_5_white));
                weeklyText.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
                yearlyText.setTextColor(ContextCompat.getColor(getContext(), R.color.black));

            }
        });

        yearlyBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                calendarView.setVisibility(View.GONE);
                calendarView_year.setVisibility(View.VISIBLE);
                weeklyBook.setBackground(getResources().getDrawable(R.drawable.corner_white_5));
                yearlyBook.setBackground(getResources().getDrawable(R.drawable.corner_red_2));
                weeklyText.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
                yearlyText.setTextColor(ContextCompat.getColor(getContext(), R.color.white));

            }
        });


        recPeoplesName.setLayoutManager(new GridLayoutManager(getContext(), 2));
        adapterPeoplesName = new AdapterPeoplesName(getContext(), new AdapterPeoplesName.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {
                openDialog(item);
            }
        });
        recPeoplesName.setAdapter(adapterPeoplesName);


        view.findViewById(R.id.newBooking).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), NewBooking1.class);
                startActivity(intent);
            }
        });

        /* start before 1 month from now */
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -1);

        /* end after 1 month from now */
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);

        final Calendar defaultSelectedDate = Calendar.getInstance();


        month.setText(DateFormat.format("MMM yyyy", Calendar.getInstance()).toString());


        HorizontalCalendar horizontalCalendar = new HorizontalCalendar.Builder(view, R.id.calendarView)
                .range(startDate, endDate)
                .datesNumberOnScreen(7)
                .configure()
                .formatTopText("EE")
                .formatMiddleText("dd")
                //   .formatBottomText("MMM")
                .selectedDateBackground(getResources().getDrawable(R.drawable.black_round_corner))
                .showTopText(true)
                .showBottomText(false)
                .textColor(Color.parseColor("#102759"), Color.WHITE)
                .colorTextMiddle(Color.parseColor("#102759"), Color.WHITE)
                .selectorColor(Color.parseColor("#0000ffff"))
                .end()

                .defaultSelectedDate(defaultSelectedDate)
                /* .addEvents(new CalendarEventsPredicate() {

                     Random rnd = new Random();
                     @Override
                     public List<CalendarEvent> events(Calendar date) {
                         List<CalendarEvent> events = new ArrayList<>();
                         int count = rnd.nextInt(6);

                         for (int i = 0; i <= count; i++){
                             events.add(new CalendarEvent(Color.rgb(rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)), "event"));
                         }

                         return events;
                     }
                 })*/
                .build();


        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar date, int position) {

                String selectedDateStr = DateFormat.format("MMM yyyy", date).toString();
                Log.i("onDateSelected", selectedDateStr + " - Position = " + position);

                month.setText(selectedDateStr);            }

        });



        return view;
    }

    private void openDialog(int position) {

        Dialog dialogForCity;
        dialogForCity = new Dialog(getContext());
        dialogForCity.setContentView(R.layout.custom_dialog_peoples_detail);
        dialogForCity.setCancelable(true);
        dialogForCity.setCanceledOnTouchOutside(true);
        dialogForCity.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialogForCity.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);


        dialogForCity.findViewById(R.id.dialogClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialogForCity.dismiss();

            }
        });


        dialogForCity.show();
    }


}