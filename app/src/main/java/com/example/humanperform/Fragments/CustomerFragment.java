package com.example.humanperform.Fragments;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.humanperform.Activity.CreateGroupPeoplesList;
import com.example.humanperform.Activity.ServiceInner;
import com.example.humanperform.Adapter.AdapterBookPeoplesList;
import com.example.humanperform.R;

public class CustomerFragment extends Fragment {

    private RecyclerView recBookPeoplesList;
    private AdapterBookPeoplesList adapterBookPeoplesList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.customer_fragment, container, false);

        recBookPeoplesList = view.findViewById(R.id.recBookPeoplesList);


        recBookPeoplesList.setLayoutManager(new GridLayoutManager(getContext(), 2));
        adapterBookPeoplesList = new AdapterBookPeoplesList(getContext(), new AdapterBookPeoplesList.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

                startActivity(new Intent(getContext(), CreateGroupPeoplesList.class));

            }
        });

        TextView customerText = view.findViewById(R.id.customerText);
        TextView groupText = view.findViewById(R.id.groupText);


        customerText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customerText.setBackground(getResources().getDrawable(R.drawable.color_round_red_10dp));
                groupText.setBackground(getResources().getDrawable(R.drawable.color_round_gray_10dp));
                customerText.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                groupText.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
            }
        });


        groupText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customerText.setBackground(getResources().getDrawable(R.drawable.color_round_gray_10dp));
                groupText.setBackground(getResources().getDrawable(R.drawable.color_round_red_10dp));
                customerText.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
                groupText.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
            }
        });


        recBookPeoplesList.setAdapter(adapterBookPeoplesList);
        return view;
    }
}