package com.example.humanperform.Fragments;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.humanperform.R;
import com.example.humanperform.Utils.TabSignInSignUpActivity;
import com.google.android.material.tabs.TabLayout;

public class Bookings extends Fragment {


    private TabLayout tabLayoutSignInUp;
    private ViewPager viewPagerSignInUp;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.bookings, container, false);



        tabLayoutSignInUp=(TabLayout)view.findViewById(R.id.tabLayoutSignInUp);
        viewPagerSignInUp=(ViewPager)view.findViewById(R.id.viewPagerSignInUp);

        tabLayoutSignInUp.setSelectedTabIndicatorColor(getResources().getColor(R.color.red));
        tabLayoutSignInUp.setSelectedTabIndicatorHeight((int) (1 * getResources().getDisplayMetrics().density));
        tabLayoutSignInUp.setTabTextColors(getResources().getColor(R.color.gray_navigation), getResources().getColor(R.color.black));

        tabLayoutSignInUp.addTab(tabLayoutSignInUp.newTab().setText("Service"));
        tabLayoutSignInUp.addTab(tabLayoutSignInUp.newTab().setText("Customer"));

        tabLayoutSignInUp.setTabGravity(TabLayout.GRAVITY_FILL);

        final TabSignInSignUpActivity pagerTabActivity = new TabSignInSignUpActivity(getContext(),getChildFragmentManager(), tabLayoutSignInUp.getTabCount());
        viewPagerSignInUp.setAdapter(pagerTabActivity);
        viewPagerSignInUp.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayoutSignInUp));

        tabLayoutSignInUp.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPagerSignInUp.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}