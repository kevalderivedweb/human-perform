package com.example.humanperform.Fragments;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.humanperform.Activity.AddLeave;
import com.example.humanperform.Activity.Filter_HP;
import com.example.humanperform.R;

import java.util.Calendar;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarView;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;

public class Schedule extends Fragment {

    private HorizontalCalendar horizontalCalendar;
    private TextView mAfternoon;
    private TextView mMorning;
    private TextView mAdd_Leave;

    private LinearLayout weeklyBook, yearlyBook;
    private TextView weeklyText, yearlyText;
    private HorizontalCalendarView calendarView;
    private CalendarView calendarView_year;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.schedule, container, false);


        mMorning = view.findViewById(R.id.morning);
        mAfternoon = view.findViewById(R.id.afternoon);
        mAdd_Leave = view.findViewById(R.id.add_new_leave);

        mMorning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMorning.setBackground(getActivity().getResources().getDrawable(R.drawable.bottamline_fill));
                mAfternoon.setBackground(getActivity().getResources().getDrawable(R.drawable.bottamline_unfill));
                mMorning.setPadding(5, 5, 5, 5);
                mAfternoon.setPadding(5, 5, 5, 5);


            }
        });

        mAfternoon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAfternoon.setBackground(getActivity().getResources().getDrawable(R.drawable.bottamline_fill));
                mMorning.setBackground(getActivity().getResources().getDrawable(R.drawable.bottamline_unfill));
                mMorning.setPadding(5, 5, 5, 5);
                mAfternoon.setPadding(5, 5, 5, 5);


            }
        });


        mAdd_Leave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), AddLeave.class);
                startActivity(intent);

            }
        });

        calendarView = view.findViewById(R.id.calendarView);
        calendarView_year = view.findViewById(R.id.calendarView_year);


        weeklyBook = view.findViewById(R.id.weeklyBook);
        yearlyBook = view.findViewById(R.id.yearlyBook);
        weeklyText = view.findViewById(R.id.weeklyText);
        yearlyText = view.findViewById(R.id.yearlyText);


        calendarView.setVisibility(View.VISIBLE);
        calendarView_year.setVisibility(View.GONE);


        weeklyBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                calendarView.setVisibility(View.VISIBLE);
                calendarView_year.setVisibility(View.GONE);
                weeklyBook.setBackground(getResources().getDrawable(R.drawable.corner_round_5));
                yearlyBook.setBackground(getResources().getDrawable(R.drawable.corner_round_5_white));
                weeklyText.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
                yearlyText.setTextColor(ContextCompat.getColor(getContext(), R.color.black));

            }
        });

        yearlyBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                calendarView.setVisibility(View.GONE);
                calendarView_year.setVisibility(View.VISIBLE);
                weeklyBook.setBackground(getResources().getDrawable(R.drawable.corner_white_5));
                yearlyBook.setBackground(getResources().getDrawable(R.drawable.corner_red_2));
                weeklyText.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
                yearlyText.setTextColor(ContextCompat.getColor(getContext(), R.color.white));

            }
        });




        /* start 2 months ago from now */
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -2);

        /* end after 2 months from now */
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 2);

        // Default Date set to Today.
        final Calendar defaultSelectedDate = Calendar.getInstance();

        horizontalCalendar = new HorizontalCalendar.Builder(view, R.id.calendarView)
                .range(startDate, endDate)
                .datesNumberOnScreen(7)
                .configure()
                .formatTopText("EE")
                .formatMiddleText("dd")
                //    .formatBottomText("MMM")
                .selectedDateBackground(getResources().getDrawable(R.drawable.black_round_corner))
                .showTopText(true)
                .showBottomText(false)
                .textColor(Color.parseColor("#102759"), Color.WHITE)
                .colorTextMiddle(Color.parseColor("#102759"), Color.WHITE)
                .selectorColor(Color.parseColor("#0000ffff"))
                .end()
                .defaultSelectedDate(defaultSelectedDate)
                /* .addEvents(new CalendarEventsPredicate() {

                     Random rnd = new Random();
                     @Override
                     public List<CalendarEvent> events(Calendar date) {
                         List<CalendarEvent> events = new ArrayList<>();
                         int count = rnd.nextInt(6);

                         for (int i = 0; i <= count; i++){
                             events.add(new CalendarEvent(Color.rgb(rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)), "event"));
                         }

                         return events;
                     }
                 })*/
                .build();


        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar date, int position) {

                /*String selectedDateStr = DateFormat.format("MMM yyyy", date).toString();
                month.setText(selectedDateStr);

                String select = DateFormat.format("EEE, MMM d, yyyy", date).toString();
                Toast.makeText(getContext(), select + "", Toast.LENGTH_SHORT).show();*/

            }

        });


        return view;
    }


    @Override
    public void onResume() {
        super.onResume();

    }
}