package com.example.humanperform.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.humanperform.Adapter.AdapterBookPeoplesList;
import com.example.humanperform.Adapter.AdapterServiceInner;
import com.example.humanperform.R;

public class ServiceInner extends AppCompatActivity {

    private RecyclerView recServiceInner, recBookPeoplesList;
    private AdapterServiceInner adapterServiceInner;
    private AdapterBookPeoplesList adapterBookPeoplesList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_inner);

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        recServiceInner = findViewById(R.id.recServiceInner);
        recServiceInner.setLayoutManager(new LinearLayoutManager(this));
        adapterServiceInner = new AdapterServiceInner(this, new AdapterServiceInner.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {
                openCustomersDialog(item);
            }
        });
        recServiceInner.setAdapter(adapterServiceInner);
    }

    private void openCustomersDialog(int item) {


        Dialog dialogForCity;
        dialogForCity = new Dialog(this);
        dialogForCity.setContentView(R.layout.custom_dialog_customers);
        dialogForCity.setCancelable(true);
        dialogForCity.setCanceledOnTouchOutside(true);
        dialogForCity.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialogForCity.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);


        recBookPeoplesList = dialogForCity.findViewById(R.id.recBookPeoplesList);
        recBookPeoplesList.setLayoutManager(new GridLayoutManager(this, 2));
        adapterBookPeoplesList = new AdapterBookPeoplesList(this, new AdapterBookPeoplesList.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {



            }
        });
        recBookPeoplesList.setAdapter(adapterBookPeoplesList);



        TextView customerText = dialogForCity.findViewById(R.id.customerText);
        TextView groupText = dialogForCity.findViewById(R.id.groupText);


        customerText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customerText.setBackground(getResources().getDrawable(R.drawable.color_round_red_10dp));
                groupText.setBackground(getResources().getDrawable(R.drawable.color_round_gray_10dp));
                customerText.setTextColor(ContextCompat.getColor(ServiceInner.this, R.color.white));
                groupText.setTextColor(ContextCompat.getColor(ServiceInner.this, R.color.black));
            }
        });


        groupText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customerText.setBackground(getResources().getDrawable(R.drawable.color_round_gray_10dp));
                groupText.setBackground(getResources().getDrawable(R.drawable.color_round_red_10dp));
                customerText.setTextColor(ContextCompat.getColor(ServiceInner.this, R.color.black));
                groupText.setTextColor(ContextCompat.getColor(ServiceInner.this, R.color.white));
            }
        });




        dialogForCity.findViewById(R.id.btnSignProduct).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(dialogForCity.getContext(), TimeSchedule.class));
            }
        });
        dialogForCity.findViewById(R.id.dialogClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialogForCity.dismiss();

            }
        });
        dialogForCity.show();
    }
}