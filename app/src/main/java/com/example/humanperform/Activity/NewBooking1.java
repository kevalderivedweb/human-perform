package com.example.humanperform.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.humanperform.Adapter.AdapterBookPeoplesList;
import com.example.humanperform.R;

public class NewBooking1 extends AppCompatActivity {

    private RecyclerView recBookPeoplesList;
    private AdapterBookPeoplesList adapterBookPeoplesList;

    private TextView customerText, groupText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_booking1);


        recBookPeoplesList = findViewById(R.id.recBookPeoplesList);
        customerText = findViewById(R.id.customerText);
        groupText = findViewById(R.id.groupText);


        customerText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customerText.setBackground(getResources().getDrawable(R.drawable.color_round_red_10dp));
                groupText.setBackground(getResources().getDrawable(R.drawable.color_round_gray_10dp));
                customerText.setTextColor(ContextCompat.getColor(NewBooking1.this, R.color.white));
                groupText.setTextColor(ContextCompat.getColor(NewBooking1.this, R.color.black));
            }
        });


        groupText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customerText.setBackground(getResources().getDrawable(R.drawable.color_round_gray_10dp));
                groupText.setBackground(getResources().getDrawable(R.drawable.color_round_red_10dp));
                customerText.setTextColor(ContextCompat.getColor(NewBooking1.this, R.color.black));
                groupText.setTextColor(ContextCompat.getColor(NewBooking1.this, R.color.white));
            }
        });


        recBookPeoplesList.setLayoutManager(new GridLayoutManager(this, 2));
        adapterBookPeoplesList = new AdapterBookPeoplesList(this, new AdapterBookPeoplesList.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {


            }
        });
        recBookPeoplesList.setAdapter(adapterBookPeoplesList);


        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        findViewById(R.id.btnNewBooking1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewBooking1.this, NewBooking2.class);
                startActivity(intent);            }
        });


    }
}