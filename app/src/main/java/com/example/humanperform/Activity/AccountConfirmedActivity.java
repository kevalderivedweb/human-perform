package com.example.humanperform.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.example.humanperform.R;

public class AccountConfirmedActivity extends AppCompatActivity {

    private TextView textConfirmedAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_confirmed);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        textConfirmedAccount = findViewById(R.id.textConfirmedAccount);

        textConfirmedAccount.setText(Html.fromHtml("<p><span style=\"color: #ff0000;\">Congratulations</span><span style=\"color: #000000;\"> your account has been confirmed</span></p>"));


        findViewById(R.id.btnAccountConfirmed).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AccountConfirmedActivity.this, LoginActivity.class));
            }
        });

    }
}