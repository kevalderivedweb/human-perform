package com.example.humanperform.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.humanperform.Adapter.AdapterCustomersDetails;
import com.example.humanperform.R;

public class CustomersDetails extends AppCompatActivity {

    private RecyclerView recBookedActive;
    private AdapterCustomersDetails adapterCustomersDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customers_details);


        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        findViewById(R.id.paymentStatus).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CustomersDetails.this, PaymentStatus.class));
            }
        });



        findViewById(R.id.btnSubmitDoc).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CustomersDetails.this, NextBookingDate.class));
            }
        });




        recBookedActive = findViewById(R.id.recBookedActive);
        recBookedActive.setLayoutManager(new LinearLayoutManager(CustomersDetails.this));
        adapterCustomersDetails = new AdapterCustomersDetails(CustomersDetails.this, new AdapterCustomersDetails.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

            }
        });
        recBookedActive.setAdapter(adapterCustomersDetails);



    }
}