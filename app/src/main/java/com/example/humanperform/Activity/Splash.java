package com.example.humanperform.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

import com.example.humanperform.R;

import java.util.Random;

public class Splash extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        int secondsDelayed = 1;
        new Handler().postDelayed(new Runnable() {
            public void run() {
                startActivity(new Intent(Splash.this, IntroActivity.class));
                finish();
            }
        }, secondsDelayed * 500);

       /* Random random = new Random(System.currentTimeMillis());
        int posOfImage = random.nextInt(yourListOfImages.length);
        ImageView imageView= (ImageView) findViewById(R.id.splashImage);
        imageView.setImageResource(yourListOfImages[posOfImage]);*/


    }


}