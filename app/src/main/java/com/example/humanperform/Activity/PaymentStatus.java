package com.example.humanperform.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import com.example.humanperform.Adapter.AdapterPaymentStatus;
import com.example.humanperform.R;

public class PaymentStatus extends AppCompatActivity {

    private RecyclerView recPaymentStatus;
    private AdapterPaymentStatus adapterPaymentStatus;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_status);

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        recPaymentStatus = findViewById(R.id.recPaymentStatus);
        recPaymentStatus.setLayoutManager(new LinearLayoutManager(PaymentStatus.this));
        adapterPaymentStatus = new AdapterPaymentStatus(PaymentStatus.this, new AdapterPaymentStatus.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

            }
        });
        recPaymentStatus.setAdapter(adapterPaymentStatus);


    }



}