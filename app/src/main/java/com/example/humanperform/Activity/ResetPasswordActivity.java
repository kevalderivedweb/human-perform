package com.example.humanperform.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.humanperform.R;

public class ResetPasswordActivity extends AppCompatActivity {

    private ImageView icon_password_visible, icon_password_invisible, icon_password_visible_confirm, icon_password_invisible_confirm;
    private EditText passwordReset, confirmPasswordReset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        icon_password_visible = findViewById(R.id.icon_password_visible);
        icon_password_invisible = findViewById(R.id.icon_password_invisible);
        icon_password_visible_confirm = findViewById(R.id.icon_password_visible_confirm);
        icon_password_invisible_confirm = findViewById(R.id.icon_password_invisible_confirm);
        passwordReset = findViewById(R.id.passwordReset);
        confirmPasswordReset = findViewById(R.id.confirmPasswordReset);


        icon_password_visible.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                icon_password_visible.setVisibility(View.GONE);
                icon_password_invisible.setVisibility(View.VISIBLE);

                passwordReset.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                passwordReset.setSelection(passwordReset.length());

                passwordReset.setTransformationMethod(HideReturnsTransformationMethod.getInstance());

            }
        });

        icon_password_invisible.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                passwordReset.setInputType(InputType.TYPE_CLASS_TEXT);
                passwordReset.setSelection(passwordReset.length());
                passwordReset.setTransformationMethod(PasswordTransformationMethod.getInstance());


                icon_password_invisible.setVisibility(View.GONE);
                icon_password_visible.setVisibility(View.VISIBLE);
            }
        });



        icon_password_visible_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                icon_password_visible_confirm.setVisibility(View.GONE);
                icon_password_invisible_confirm.setVisibility(View.VISIBLE);

                confirmPasswordReset.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                confirmPasswordReset.setSelection(confirmPasswordReset.length());

                confirmPasswordReset.setTransformationMethod(HideReturnsTransformationMethod.getInstance());

            }
        });

        icon_password_invisible_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmPasswordReset.setInputType(InputType.TYPE_CLASS_TEXT);
                confirmPasswordReset.setSelection(confirmPasswordReset.length());
                confirmPasswordReset.setTransformationMethod(PasswordTransformationMethod.getInstance());


                icon_password_invisible_confirm.setVisibility(View.GONE);
                icon_password_visible_confirm.setVisibility(View.VISIBLE);
            }
        });



    }
}