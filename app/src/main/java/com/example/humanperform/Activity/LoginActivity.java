package com.example.humanperform.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.humanperform.R;

public class LoginActivity extends AppCompatActivity {

    private ImageView icon_password_visible_login, icon_password_invisible_login;
    private EditText mPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        icon_password_visible_login = findViewById(R.id.icon_password_visible_login);
        icon_password_invisible_login = findViewById(R.id.icon_password_invisible_login);
        mPassword = findViewById(R.id.passwordLogin);


        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.btnLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
            }
        });


        icon_password_visible_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                icon_password_visible_login.setVisibility(View.GONE);
                icon_password_invisible_login.setVisibility(View.VISIBLE);

                mPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                mPassword.setSelection(mPassword.length());

                mPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());

            }
        });

        icon_password_invisible_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                mPassword.setSelection(mPassword.length());
                mPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());


                icon_password_invisible_login.setVisibility(View.GONE);
                icon_password_visible_login.setVisibility(View.VISIBLE);
            }
        });

        findViewById(R.id.btn2Register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
                finish();
            }
        });

        findViewById(R.id.forgotPassowordText).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, ForgotPassowrdActivity.class));
            }
        });
    }
}