package com.example.humanperform.Activity;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.humanperform.Fragments.Bookings;
import com.example.humanperform.Fragments.HP;
import com.example.humanperform.Fragments.More;
import com.example.humanperform.Fragments.Plan;
import com.example.humanperform.Fragments.Schedule;
import com.example.humanperform.R;

public class MainActivity extends AppCompatActivity {

    private LinearLayout navLinear1, navLinear2, navLinear3, navLinear4, navLinear5;
    private ImageView image1, image2, image3, image4, image5;
    private TextView text1, text2, text3, text4, text5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setFooterNavigation();


        findViewById(R.id.notification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, NotificationActivity.class));
            }
        });



    }


    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .disallowAddToBackStack()
                .commit();
    }


    private void setFooterNavigation(){

        navLinear1 = findViewById(R.id.navLinear1);
        navLinear2 = findViewById(R.id.navLinear2);
        navLinear3 = findViewById(R.id.navLinear3);
        navLinear4 = findViewById(R.id.navLinear4);
        navLinear5 = findViewById(R.id.navLinear5);
        image1 = findViewById(R.id.image1);
        image2 = findViewById(R.id.image2);
        image3 = findViewById(R.id.image3);
        image4 = findViewById(R.id.image4);
        image5 = findViewById(R.id.image5);
        text1 = findViewById(R.id.text1);
        text2 = findViewById(R.id.text2);
        text3 = findViewById(R.id.text3);
        text4 = findViewById(R.id.text4);
        text5 = findViewById(R.id.text5);


        Plan plan = new Plan();
        replaceFragment(R.id.fragmentLinearHome, plan, "plan");


        navLinear1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Plan plan = new Plan();
                replaceFragment(R.id.fragmentLinearHome, plan, "plan");

                image1.setImageResource(R.drawable.home_active);
                image2.setImageResource(R.drawable.hp);
                image3.setImageResource(R.drawable.schedule);
                image4.setImageResource(R.drawable.bookings);
                image5.setImageResource(R.drawable.more);

                text1.setTextColor(getResources().getColor(R.color.red));
                text2.setTextColor(getResources().getColor(R.color.gray_navigation));
                text3.setTextColor(getResources().getColor(R.color.gray_navigation));
                text4.setTextColor(getResources().getColor(R.color.gray_navigation));
                text5.setTextColor(getResources().getColor(R.color.gray_navigation));
            }
        });

        navLinear2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HP humanPerform = new HP();
                replaceFragment(R.id.fragmentLinearHome, humanPerform, "human_perform");

                image1.setImageResource(R.drawable.home);
                image2.setImageResource(R.drawable.hp_active);
                image3.setImageResource(R.drawable.schedule);
                image4.setImageResource(R.drawable.bookings);
                image5.setImageResource(R.drawable.more);

                text1.setTextColor(getResources().getColor(R.color.gray_navigation));
                text2.setTextColor(getResources().getColor(R.color.red));
                text3.setTextColor(getResources().getColor(R.color.gray_navigation));
                text4.setTextColor(getResources().getColor(R.color.gray_navigation));
                text5.setTextColor(getResources().getColor(R.color.gray_navigation));
            }
        });

        navLinear3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Schedule schedule = new Schedule();
                replaceFragment(R.id.fragmentLinearHome, schedule, "schedule");

                image1.setImageResource(R.drawable.home);
                image2.setImageResource(R.drawable.hp);
                image3.setImageResource(R.drawable.schedule_active);
                image4.setImageResource(R.drawable.bookings);
                image5.setImageResource(R.drawable.more);

                text1.setTextColor(getResources().getColor(R.color.gray_navigation));
                text2.setTextColor(getResources().getColor(R.color.gray_navigation));
                text3.setTextColor(getResources().getColor(R.color.red));
                text4.setTextColor(getResources().getColor(R.color.gray_navigation));
                text5.setTextColor(getResources().getColor(R.color.gray_navigation));
            }
        });

        navLinear4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bookings bookings = new Bookings();
                replaceFragment(R.id.fragmentLinearHome, bookings, "bookings");

                image1.setImageResource(R.drawable.home);
                image2.setImageResource(R.drawable.hp);
                image3.setImageResource(R.drawable.schedule);
                image4.setImageResource(R.drawable.bookings_active);
                image5.setImageResource(R.drawable.more);

                text1.setTextColor(getResources().getColor(R.color.gray_navigation));
                text2.setTextColor(getResources().getColor(R.color.gray_navigation));
                text3.setTextColor(getResources().getColor(R.color.gray_navigation));
                text4.setTextColor(getResources().getColor(R.color.red));
                text5.setTextColor(getResources().getColor(R.color.gray_navigation));
            }
        });

        navLinear5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                More more = new More();
                replaceFragment(R.id.fragmentLinearHome, more, "more");

                image1.setImageResource(R.drawable.home);
                image2.setImageResource(R.drawable.hp);
                image3.setImageResource(R.drawable.schedule);
                image4.setImageResource(R.drawable.bookings);
                image5.setImageResource(R.drawable.more_active);

                text1.setTextColor(getResources().getColor(R.color.gray_navigation));
                text2.setTextColor(getResources().getColor(R.color.gray_navigation));
                text3.setTextColor(getResources().getColor(R.color.gray_navigation));
                text4.setTextColor(getResources().getColor(R.color.gray_navigation));
                text5.setTextColor(getResources().getColor(R.color.red));
            }
        });
    }

}