package com.example.humanperform.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import com.example.humanperform.Adapter.AdapterNotification;
import com.example.humanperform.R;

public class NotificationActivity extends AppCompatActivity {

    private RecyclerView recListNotification;
    private AdapterNotification adapterNotification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        recListNotification = findViewById(R.id.recListNotification);
        recListNotification.setLayoutManager(new LinearLayoutManager(this));
        adapterNotification = new AdapterNotification(this, new AdapterNotification.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

            }
        });
        recListNotification.setAdapter(adapterNotification);



    }
}