package com.example.humanperform.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import com.example.humanperform.Adapter.AdapterNextBookingDate;
import com.example.humanperform.R;

public class NextBookingDate extends AppCompatActivity {

    private RecyclerView recNextBookingDate;
    private AdapterNextBookingDate adapterNextBookingDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next_booking_date);


        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        recNextBookingDate = findViewById(R.id.recNextBookingDate);
        recNextBookingDate.setLayoutManager(new LinearLayoutManager(NextBookingDate.this));
        adapterNextBookingDate = new AdapterNextBookingDate(NextBookingDate.this, new AdapterNextBookingDate.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

            }
        });
        recNextBookingDate.setAdapter(adapterNextBookingDate);



    }



}