package com.example.humanperform.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.humanperform.Adapter.AdapterCustomers;
import com.example.humanperform.R;

public class CreateGroup extends AppCompatActivity {

    private RecyclerView recCustomers;
    private AdapterCustomers adapterCustomers;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);


        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        recCustomers = findViewById(R.id.recCustomers);
        recCustomers.setLayoutManager(new LinearLayoutManager(CreateGroup.this, LinearLayoutManager.HORIZONTAL, false));
        adapterCustomers = new AdapterCustomers(CreateGroup.this, new AdapterCustomers.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {
                startActivity(new Intent(CreateGroup.this, CustomersDetails.class));
            }
        });
        recCustomers.setAdapter(adapterCustomers);



    }
}