package com.example.humanperform.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.humanperform.Adapter.AdapterCreateGroup;
import com.example.humanperform.R;

public class CreateGroupPeoplesList extends AppCompatActivity {

    private RecyclerView recCreateGroup;
    private AdapterCreateGroup adapterCreateGroup;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group_peoples_list);


        recCreateGroup = findViewById(R.id.recCreateGroup);


        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.btnCreteGroup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CreateGroupPeoplesList.this, CreateGroup.class));
            }
        });



        recCreateGroup.setLayoutManager(new GridLayoutManager(CreateGroupPeoplesList.this, 2));
        adapterCreateGroup = new AdapterCreateGroup(CreateGroupPeoplesList.this, new AdapterCreateGroup.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {


            }
        });
        recCreateGroup.setAdapter(adapterCreateGroup);




    }
}