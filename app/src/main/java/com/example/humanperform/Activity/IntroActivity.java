package com.example.humanperform.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.humanperform.R;
import com.example.humanperform.Utils.UserSession;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

public class IntroActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private int[] layouts;
    private Button  btnGetStarted;
    private UserSession session;
    private DotsIndicator dotsIndicator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark

        viewPager = (ViewPager) findViewById(R.id.viewPagerSliderIntro);
        // btnSkip = (Button) findViewById(R.id.btn_skip);
        btnGetStarted = (Button) findViewById(R.id.btnSlider);
        dotsIndicator = (DotsIndicator) findViewById(R.id.worm_dots_indicator);

        session = new UserSession(this);
        if (!session.isFirstTimeLaunch()) { // ! aavse after complete the design
            launchHomeScreen();
            //   finish();
        }


        layouts = new int[]{
                R.layout.slide1,
                R.layout.slide2,
                R.layout.slice3};

        PagerAdapter myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
        dotsIndicator.setViewPager(viewPager);

        btnGetStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // checking for last page
                // if last page home screen will be launched
                int current = getItem();
                if (current < layouts.length) {
                    // move to next screen
                    viewPager.setCurrentItem(current);
                } else {
                    launchHomeScreen();
                }
            }
        });


    }

    //  viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {


            // changing the next button text 'NEXT' / 'GOT IT'
        }

        @Override
        public void onPageScrolled(int position, float arg1, int arg2) {

            if (position == layouts.length - 1) {

                // last page. make button text to GOT IT
                btnGetStarted.setVisibility(View.VISIBLE);
            } else {
                // still pages are left
                btnGetStarted.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    private void launchHomeScreen() {
        session.setFirstTimeLaunch(false);
        startActivity(new Intent(this, LoginOptionActivity.class));
        finish();
    }


    public class MyViewPagerAdapter extends PagerAdapter {

        public MyViewPagerAdapter() {
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);
            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, @NonNull Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }


    private int getItem() {
        return viewPager.getCurrentItem() + 1;
    }


}