package com.example.humanperform.Utils;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.humanperform.Fragments.CustomerFragment;
import com.example.humanperform.Fragments.ServiceFragament;

public class TabSignInSignUpActivity extends FragmentPagerAdapter {

    private Context myContext;
    private int totalTabs;

    public TabSignInSignUpActivity(Context context, FragmentManager fm, int totalTabs) {
        super(fm);
        myContext = context;
        this.totalTabs = totalTabs;
    }

    // this is for fragment tabs
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                ServiceFragament serviceFragament = new ServiceFragament();
                return serviceFragament;
            case 1:
                CustomerFragment customerFragment = new CustomerFragment();
                return customerFragment;

            default:
                return null;
        }
    }

    // this counts total number of tabs
    @Override
    public int getCount() {
        return totalTabs;
    }

}
